/* global describe, it */

(function () {

    'use strict';

    describe('App By Phone Application', function () {

        beforeEach(function () {
            this.app = AppByPhone;
            this.app.start()
        });

        afterEach(function () {
            Backbone.history.stop();
            delete this.app;
        });

        describe('constructor', function () {
            it('should have a router', function () {
                this.app.should.have.property('routers');
                this.app.routers.should.have.property('mainRouter');
                this.app.routers.mainRouter.should.be.ok;
            });
            it('should have a router controller', function () {
                this.app.should.have.property('controllers');
                this.app.routers.should.be.ok;
            });
            it('should have models', function () {
                this.app.should.have.property('models');
                this.app.models.should.be.ok;
            });
            it('should have views', function () {
                this.app.should.have.property('views');
                this.app.views.should.be.ok;
            });
        });

        describe('when initialized', function () {
            xit('should instantiate router with controller');
            it('should start backbone history', function () {
                Backbone.History.started.should.equal(true);
            });
        });

        describe('when started', function () {

        });
    });

})();
