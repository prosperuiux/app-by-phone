(function (global){

    'use strict';

    describe('Offer Code Widget View', function () {

        var getOptions = function () {
            return {
                template: global.templates.offerCodeWidgetTemplate,
                model: new (Backbone.Model.extend({
                    url: '/test',
                    defaults: {'testKey':'testVal'}
                }))()
            }
        };

        beforeEach(function () {
            this.$fixture = $('fixture');
            this.$fixture.empty();
        });

        afterEach(function () {
        });

        describe('when submit is clicked', function () {

            it ('should send offer code', function () {
                var submitStub = sinon.stub(OfferCodeWidgetView.prototype, 'sendOfferCode');
                var view = new OfferCodeWidgetView(getOptions());
                view.render();
                view.ui.submitBtn.trigger('click');
                submitStub.should.have.been.calledOnce;
                submitStub.restore();
            });

        });

        describe('when offercode is sent', function () {
            before(function () {
                var requests = this.requests = [];
                // set up xhr handler
                this.xhr = sinon.useFakeXMLHttpRequest();
                this.xhr.onCreate = function (xhr) {
                    requests.push(xhr);
                };
            })
            beforeEach(function () {
                var options = getOptions();
                // validate listener
                this.validateSpy = sinon.spy(options.model, 'isValid')
                // create view
                this.view = new OfferCodeWidgetView(options);
                // render view $el
                this.view.render();
                // trigger click
                this.view.ui.submitBtn.trigger('click');
            });

            afterEach(function () {
                this.requests = [];
                this.validateSpy.restore();
                this.view.remove();
                delete this.view;
                delete this.xhr;
            });
            after(function () {
                this.xhr.restore();
            });

            it('should validate the offercode syntax', function () {
                this.validateSpy.should.have.been.calledOnce;
            });

            it('should submit offercode to endpoint', function () {
                var model = this.view.model;
                var modelSaveRequest  = this.requests[0];
                var modelValuesStr = JSON.stringify(model.attributes);
                modelSaveRequest.method.should.equal('POST');
                modelSaveRequest.url.should.equal(model.url);
                modelSaveRequest.requestBody.should.equal(modelValuesStr);
            });
        });

        describe('when model is saved', function () {

            before(function () {
                this.view = new OfferCodeWidgetView(getOptions())
                this.view.render();
            });

            after(function () {
                this.view.remove();
                delete this.view;
            });

            it('should disable the submit button', function () {
                var $btn = this.view.ui.submitBtn;
                $btn.hasClass('disabled').should.equal(true);
                $btn.attr('disabled').should.equal('disabled');
            });

            it('should add "processing" class to submit button', function () {
                this.view.ui.submitBtn.hasClass('processing').should.equal(true)
            });

            it('should disable offercode field', function () {
                var $input = view.ui.offerCodeInput;
                $input.hasClass('disabled').should.equal(true);
                $input.attr('disabled').should.equal('disabled');
            })

        });

        describe('when server responds', function () {

            it('should enable submit button');

            it('should populate register form with returned values');

        });

    });

})(window);
