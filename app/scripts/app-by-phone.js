window.AppByPhone = (function (global) {

    'use strict';

    var Backbone = global.Backbone;
    function MainRouter(){}
    function MainController(){}
    return new Backbone.Marionette.Application({
        routers: {
            mainRouter: new MainRouter(new MainController(this))
        },
        controllers: {},
        models: {},
        views: {},
        onInitializeBefore: function () {
            Backbone.history.start()
        }
    })

})(window);