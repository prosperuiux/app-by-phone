var OfferCodeWidgetView = Backbone.Marionette.ItemView.extend({

    id: 'offer-code-widget',

    className: 'widget form-horizontal',

    ui: {
        submitBtn: '#offer-widget-submit',
        offerCodeInput: '[name=offer_code]'
    },

    events: {
        'click .submit-btn': 'sendOfferCode'
    },

    modelEvents: {
        sync: 'disableForm'
    },

    initialize: function (options) {
        this.template = _.template(options.template)
    },

    onRender: function () {
        this.modelBinder = new Backbone.ModelBinder();
        this.modelBinder.bind(this.model, this.el);
    },

    disableForm: function () {
        console.log('submit')
    },

    sendOfferCode: function () {
        if (this.model.isValid(true)) {
            this.model.save();
        }
    },


});
