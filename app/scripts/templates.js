/**
 * Created by fclark on 2/5/14.
 */
define([
    'text!../templates/offer-code-widget-template.html'
], function (
    OfferCodeWidgetTemplate
) {
    return {
        offerCodeWidgetTemplate: OfferCodeWidgetTemplate
    };
});
