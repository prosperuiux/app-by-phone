require.config({
    paths: {
        text: '../bower_components/requirejs-text/text',
        templates: 'templates'
    },
    deps: ['templates'],
    callback: function (templates) {
        window.AppByPhone.start(templates);
    }
});
